import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Root from './components/Root';
import store from './configureStore'
import ws from './configureWebsocket'

ReactDOM.render(
    <Root store={store} />,
    document.getElementById('root')
);