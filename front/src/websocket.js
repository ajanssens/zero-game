import Sockette from "sockette"
import { onWebsocketConnected } from "./actions/websocket"

const Websocket = (store) => {
    let started = false
    let ws = null
    
    const start = () => {
        if (started) return

        ws = new Sockette('ws://localhost:3001/ws', {
            timeout: 5e3,
            maxAttempts: 10,
            onopen: e => setOnOpen(),
            onmessage: e => onReceive(e),
            onreconnect: e => console.log('Reconnecting...', e),
            onmaximum: e => console.log('Stop Attempting!', e),
            onclose: e => console.log('Closed!', e),
            onerror: e => console.log('Error:', e)
        });

        return ws
    }

    const setOnOpen = e => {
        store.dispatch(onWebsocketConnected())
    }

    const send = message => ws.send(message)

    const sendJson = message => ws.json(message)

    const onReceive = e => {
        let data = JSON.parse(e.data)
        console.log(data.MessageType)        
        store.dispatch({type:data.MessageType, payload:data.Content})
    }

    return {start, send, sendJson, setOnOpen}
}

export default Websocket