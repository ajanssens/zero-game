import React, { Component } from 'react';
import { withRouter } from "react-router-dom";
import "./CreateGame.css";

class CreateGame extends Component {
    constructor(props) {
        super(props)
        this.state = {inputValue: ''}
        this.inputChange = this.inputChange.bind(this);
    }

    inputChange(event) {
        this.setState({inputValue: event.target.value})
    }

    render() {
        const inputClasses = this.state.inputValue !== '' ? "effect-16 has-content" : "effect-16"
        const buttonClasses = this.state.inputValue !== '' ? 'button' : 'hidden' 

        return (
            
            <div id="CreateGameContainer">
            
                <div className="col-3 input-effect">
                    <input value={this.state.inputValue} onChange={this.inputChange} className={inputClasses} type="text" placeholder="" />
                    <label>How many payers ?</label>
                    <span className="focus-border"></span>
                </div>
                <input onClick={() => this.createGame(this.state.inputValue)} className={buttonClasses} type="button" value="Push Me!" />
            </div>
        )
    }
}

export default withRouter(CreateGame);