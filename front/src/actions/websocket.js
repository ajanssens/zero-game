export const ON_WEBSOCKET_CONNECTED = 'ON_WEBSOCKET_CONNECTED'
export const onWebsocketConnected = () => ({type: ON_WEBSOCKET_CONNECTED})

export const TOKEN = 'TOKEN'
export const onReceiveToken = (payload) => ({type: TOKEN, payload})