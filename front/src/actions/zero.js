export const SET_HAND = 'SET_HAND'
export const SET_TABLE = 'SET_TABLE'
export const SELECT_HAND_CARD = 'SELECT_HAND_CARD'
export const SELECT_TABLE_CARD = 'SELECT_TABLE_CARD'
export const SET_NICKNAME = 'SET_NICKNAME'

export const setHand = cards => ({type: SET_HAND, cards})
export const setTable = cards => ({type: SET_TABLE, cards})
export const selectHandCard = index => ({ type: SELECT_HAND_CARD, index })
export const selectTableCard = index => ({ type: SELECT_TABLE_CARD, index })
export const setNickname = nickname => ({type: SET_NICKNAME, nickname})