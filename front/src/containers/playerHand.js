import React, { Component } from 'react'
import { connect } from 'react-redux'
import Card from '../components/Card'
import { selectHandCard } from './../actions/zero'

class PlayerHand extends Component {
    render() {
        return (
            <div className="hand">
                {this.props.cards.map((element, index) => {
                    let isSelected = false;

                    if (this.props.selectedHandCard === index) {
                        isSelected = true;
                    }

                    return <Card 
                        isSelected={isSelected}
                        onClick={() => this.props.onCardClick(index)} 
                        key={`card-hand-${index}`} 
                        index={index}
                        value={element.Value} 
                        color={element.Color} />
                })}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        cards: state.hand,
        selectedHandCard: state.selectedHandCard
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onCardClick: id => {
            dispatch(selectHandCard(id))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PlayerHand)