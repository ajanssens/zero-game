import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom";
import './ChooseNickname.css';
import { setNickname } from '../actions/zero';

const mapStateToProps = (store) => {
    return {
        "nickname": store.nickname
    }
}

class ChooseNickname extends Component {
    constructor(props) {
        super(props)
        this.state = {inputValue: ''}
        this.inputChange = this.inputChange.bind(this);
    }

    inputChange(event) {
        this.setState({inputValue: event.target.value})
    }

    saveNickname(nickname) {
        if (nickname !== '') {
            this.props.dispatch(setNickname(nickname))
            this.props.history.push("/create")
        }
    }

    render() {
        const inputClasses = this.state.inputValue !== '' ? "effect-16 has-content" : "effect-16"
        const buttonClasses = this.state.inputValue !== '' ? 'button' : 'hidden' 

        return (
            <div id="chooseNicknameContainer">
                <div className="col-3 input-effect">
                    <input value={this.state.inputValue} onChange={this.inputChange} className={inputClasses} type="text" placeholder="" />
                    <label>What is your nickname ?</label>
                    <span className="focus-border"></span>
                </div>
                <input onClick={() => this.saveNickname(this.state.inputValue)} className={buttonClasses} type="button" value="Push Me!" />
            </div>
        )
    }
}

export default withRouter(connect(mapStateToProps)(ChooseNickname));