import React, { Component, Fragment } from 'react';
import TableCards from './tableCards'
import PlayerHand from './playerHand'
import Spinner from '../components/Spinner'
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
  return {
    websocketConnected: state.websocketConnected
  }
}

function GameReady() {
  return (
    <Fragment>
      <TableCards />
      <PlayerHand />
    </Fragment>
  )
}

function LoadingGame() {
  return (
    <Spinner />
  )
}

function GameView(props) {
  const isGameReady = props.isGameReady

  if (isGameReady) {
    return <GameReady />
  }

  return <LoadingGame />
}

class Game extends Component {

  render() {
    const { websocketConnected } = this.props

    return (
      <div className="game">
        <GameView isGameReady={websocketConnected} />
      </div>
    )
  }
}

export default connect(mapStateToProps)(Game);