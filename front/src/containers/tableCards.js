import React, { Component } from 'react'
import { connect } from 'react-redux'
import Card from '../components/Card'
import { selectTableCard } from './../actions/zero'

class TableCards extends Component {
    render() {
        return (
            <div className="hand">
                {this.props.cards.map((element, index) => {
                    let isSelected = false;

                    if (this.props.selectedTableCard === index) {
                        isSelected = true;
                    }

                    return <Card 
                        isSelected={isSelected}
                        onClick={() => this.props.onCardClick(index)} 
                        key={`card-table-${index}`} 
                        index={index}
                        value={element.Value} 
                        color={element.Color} />
                })}
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        cards: state.table,
        selectedTableCard: state.selectedTableCard
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onCardClick: id => {
            dispatch(selectTableCard(id))
        }
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TableCards)