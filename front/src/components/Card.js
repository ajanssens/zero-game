import React, { Component } from 'react';
import './card.css';

class Card extends Component {
    render() {
        let className = this.props.isSelected ? "card active" : "card";

        return (
            <div onClick={this.props.onClick} className={className}>
                <div className={this.props.color}>
                    <div className="circle">{this.props.value}</div>
                </div>
            </div>
        )
    }
}

export default Card