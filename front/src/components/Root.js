import React, { Fragment } from 'react';
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import ChooseNickname from '../containers/chooseNickname'
import Game from '../containers/Game'
import PropTypes from 'prop-types'

const Root = ({ store }) => (
    <Provider store={store}>
        <Router>
            <Fragment>
                <Route exact path="/" component={ChooseNickname} />
                <Route path="/game" component={Game} />
            </Fragment>
        </Router>
    </Provider>
)

Root.propTypes = {
    store: PropTypes.object.isRequired
}

export default Root