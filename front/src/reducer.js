import { 
    SET_HAND,
    SET_TABLE,
    SELECT_HAND_CARD,
    SELECT_TABLE_CARD,
    SET_NICKNAME,
} from "./actions/zero"

import { ON_WEBSOCKET_CONNECTED, TOKEN } from "./actions/websocket"

const initialState = {
    hand: [
        {Color: "red", Value:1},
        {Color: "green", Value:1},
        {Color: "yellow", Value:1},
        {Color: "red", Value:5},
        {Color: "red", Value:6},
        {Color: "red", Value:7},        
    ],
    table: [],
    selectedHandCard: null,
    selectedTableCard: null,
    websocketConnected: false,
    token: null,
    nickname: "Guest"
}

export function zeroApp(state = initialState, action) {
    switch (action.type) {
        case SET_TABLE:
            let table = JSON.parse(action.payload)
            return Object.assign({}, state, {
                table: table
            })
        case SET_HAND:
            let hand = JSON.parse(action.payload)
            return Object.assign({}, state, {
                hand: hand
            })
        case SELECT_HAND_CARD:
            if (state.selectedHandCard === action.index) {
                return Object.assign({}, state, {
                    selectedHandCard: null
                })
            }

            return Object.assign({}, state, {
                selectedHandCard: action.index
            })
        case SELECT_TABLE_CARD:
            if (state.selectedTableCard === action.index) {
                return Object.assign({}, state, {
                    selectedTableCard: null
                })
            }

            return Object.assign({}, state, {
                selectedTableCard: action.index
            })
        case ON_WEBSOCKET_CONNECTED:
            return Object.assign({}, state, {
                websocketConnected: true
            })
        case TOKEN:
            return Object.assign({}, state, {
                token: action.payload
            })
        case SET_NICKNAME:
            return Object.assign({}, state, {
                nickname: action.nickname
            })
        default:
            return state
    }
}
