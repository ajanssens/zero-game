import store from './configureStore'
import Websocket from './websocket'

let ws = Websocket(store)
ws.start()

export default ws