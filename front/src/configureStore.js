import { createStore } from 'redux';
import { zeroApp } from './reducer'

const store = createStore(
    zeroApp,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

export default store