package zero

var CardsPerPlayer int = 9

// GameID represents a game identifier.
type GameID string

// Game represents a zero game.
type Game struct {
	ID           GameID   `json:"id,omitempty"`
	MaxPlayers   int      `json:"max_players,omitempty"`
	CurrentRound int      `json:"current_round,omitempty"`
	Rounds       []*Round `json:"rounds,omitempty"`
	GameStarted  bool     `json:"game_started,omitempty"`
}

type GameService interface {
	Create(id GameID, maxPlayers int, currentRound int) *Game
	AddRound(game *Game, round Round) *Game
	UpdateCurrentRound(game *Game, currentRound int) *Game
	StartGame(game *Game, gameStatus bool) *Game
}

// Round represents a round in a zero game.
type Round struct {
	PlayerTurn int      `json:"player_turn,omitempty"`
	Turn       int      `json:"turn,omitempty"`
	LastTurn   int      `json:"last_turn,omitempty"`
	Cards      []Card   `json:"cards,omitempty"`
	Knocks     []Player `json:"knocks,omitempty"`
	Table      []Card   `json:"table,omitempty"`
	Players    []Player `json:"players,omitempty"`
	IsFinished bool     `json:"is_finished,omitempty"`
}

type RoundService interface {
	Create() *Round
	StartRound(round *Round) *Round
	AddPlayer(round *Round, player Player) *Round
	SwitchCard(player Player, round Round, cardFromHand int, cardFromTable int)
	NextTurn(round *Round) *Round
}

// Player represents a player in a game.
type Player struct {
	Name  string `json:"name,omitempty"`
	Hand  []Card `json:"hand,omitempty"`
	Score Score  `json:"score,omitempty"`
}

type PlayerService interface {
	Create(name string, hand []Card, score Score) *Player
	UpdateScore() *Player
}

// Score represents the amount of points from a collection of cards.
type Score int

type ScoreService interface {
	CalculateScore(hand []Card) int
}

// Card represent a zero game card.
// Cards can have value from 1 to 8.
// Cards can have colors of the following : brown, red, yellow, grey, pink, blue, green.
type Card struct {
	Value int    `json:"value,omitempty"`
	Color string `json:"color,omitempty"`
}

// CardService represents a service for managing cards.
type CardService interface {
	Create(value int, color string) *Card
	GenerateAllCards() []Card
}
