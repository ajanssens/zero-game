package main

import (
	"encoding/json"
)

func createGame(amountOfPlayers int) {
	h.registerGame <- NewGame(amountOfPlayers)
}

func sendTable(c *WSClient) {
	currentRound := h.games[0].CurrentRound
	tableJSON, _ := json.Marshal(h.games[0].Rounds[currentRound].Table)
	tableMessage := wsMessage{MessageType: "SET_TABLE", Content: string(tableJSON)}
	t, _ := json.Marshal(tableMessage)
	c.send <- []byte(t)
}

func handleMessage(pm privateMessage) {
	//messageContent := pm.Message.Content
	messageType := pm.Message.MessageType

	switch messageType {
	case "GET_TABLE":

		message := wsMessage{MessageType: "SET_TABLE", Content: "testContent"}
		b, _ := json.Marshal(message)
		pm.client.send <- []byte(b)
		break
	}
}
