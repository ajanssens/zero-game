package main

import (
	"bufio"
	"fmt"
	"os"

	"github.com/davecgh/go-spew/spew"

	"gitlab.com/ajanssens/zero-game/zero/game"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	game := game.New(5)
	fmt.Println(game.ID)

	for {
		currentRound := &game.Rounds[game.CurrentRound]
		currentPlayer := currentRound.Players[currentRound.PlayerTurn]
		fmt.Print(currentPlayer.Name + ": ")

		switch command, _ := reader.ReadString('\n'); command {
		case "table\n":
			spew.Dump(currentRound.Table)
		case "hand\n":
			for k, card := range currentPlayer.Hand {
				spew.Dump(k, card)
			}
		case "next\n":
			game.NextTurn()
		case "sc\n":
			game.SwitchCard(0, 0)
			game.NextTurn()
		case "knock\n":
			currentRound.Knock(currentPlayer)
			game.NextTurn()
		}
	}
}
