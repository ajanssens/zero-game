package main

import (
	"log"

	"github.com/google/uuid"
)

type Game struct {
	ID           uuid.UUID
	MaxPlayers   int
	CurrentRound int
	Rounds       []Round
	GameStarted  bool
}

func (game *Game) NextTurn() *Game {
	currentRound := &game.Rounds[game.CurrentRound]
	isLastTurn := currentRound.NextTurn()
	isLastRound := game.CurrentRound == game.MaxPlayers-1

	if isLastTurn && isLastRound {
		game.GameStarted = false
		return game
	}

	if isLastTurn {
		log.Println("Starting round")
		log.Print(game.CurrentRound + 1)
		game.NewRound()
	}

	return game
}

func (game *Game) NewRound() *Game {
	round := CreateRound(game.MaxPlayers)
	game.Rounds = append(game.Rounds, round)
	game.CurrentRound = game.CurrentRound + 1
	game.Rounds[game.CurrentRound].PlayerTurn = game.CurrentRound

	return game
}

func (game *Game) SwitchCard(playerCard int, tableCard int) *Game {
	currentRound := &game.Rounds[game.CurrentRound]
	currentPlayer := currentRound.Players[currentRound.PlayerTurn]

	currentPlayer.Hand[playerCard], currentRound.Table[tableCard] = currentRound.Table[tableCard], currentPlayer.Hand[playerCard]
	currentPlayer.Score = CalculateScore(currentPlayer.Hand)

	return game
}

func (game *Game) AddPlayer() *Game {
	currentRound := &game.Rounds[game.CurrentRound]
	currentRound.AddPlayer()

	return game
}
