package main

const cardsPerPlayer int = 9

type Round struct {
	PlayerTurn int
	Turn       int
	LastTurn   int
	Knocks     []Player
	Table      []Card
	Cards      []Card
	Players    []Player
	Finished   bool
}

func (round *Round) NextTurn() bool {
	if round.LastTurn == round.Turn {
		round.Finish()
		return true
	}

	if round.PlayerTurn == len(round.Players)-1 {
		round.PlayerTurn = 0
	} else {
		round.PlayerTurn = round.PlayerTurn + 1
	}

	round.Turn = round.Turn + 1
	return false
}

func (round *Round) Knock(player Player) {
	round.Knocks = append(round.Knocks, player)
	if len(round.Knocks) == 2 {
		round.LastTurn = len(round.Players) + round.Turn - 1
	}
}

func (round *Round) Finish() {
	round.Finished = true
}
