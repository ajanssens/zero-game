package websocket

import (
	"encoding/json"
	"log"

	"gitlab.com/ajanssens/zero-game/zero"
)

const (
	listGames  = "listGames"
	createGame = "createGame"
	joinGame   = "joinGame"
)

func handleReceivedMessage(m wsMessage) {
	switch m.MessageType {
	case listGames:
		gameList := H.games
		json, _ := json.Marshal(gameList)
		m.Client.send <- json
		break
	case createGame:
		log.Println("createGame issued")
		gs := GameService{}
		gs.Create(5)
		break
	case joinGame:
		log.Println("Join game")
		gameToJoin := m.Content

		ps := PlayerService{}
		sc := ScoreService{}
		rs := RoundService{}

		game := H.games[gameToJoin]
		round := game.Rounds[game.CurrentRound]

		currentPlayerCount := len(round.Players)
		start := currentPlayerCount * zero.CardsPerPlayer
		end := (currentPlayerCount + 1) * zero.CardsPerPlayer
		hand := round.Cards[start:end]

		score := sc.CalculateScore(hand)

		player := ps.Create("playerName", hand, score)

		rs.AddPlayer(round, *player)
		break
	}
}
