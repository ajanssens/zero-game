package websocket

import (
	"testing"

	"gitlab.com/ajanssens/zero-game/zero"
)

func TestCalculateScoreZero(t *testing.T) {
	sc := ScoreService{}
	hand := []zero.Card{
		{Value: 5, Color: "brown"},
		{Value: 5, Color: "green"},
		{Value: 5, Color: "yellow"},
		{Value: 5, Color: "grey"},
		{Value: 5, Color: "red"},
		{Value: 1, Color: "red"},
		{Value: 2, Color: "red"},
		{Value: 3, Color: "red"},
		{Value: 4, Color: "red"},
	}

	expected := zero.Score(0)
	actual := sc.CalculateScore(hand)

	if actual != expected {
		t.Error("Error")
	}
}

func TestCalculateWithSameValues(t *testing.T) {
	sc := ScoreService{}
	hand := []zero.Card{
		{Value: 8, Color: "red"},
		{Value: 2, Color: "red"},
		{Value: 5, Color: "red"},
		{Value: 3, Color: "red"},
		{Value: 2, Color: "red"},
		{Value: 8, Color: "yellow"},
		{Value: 8, Color: "pink"},
		{Value: 5, Color: "yellow"},
		{Value: 5, Color: "brown"},
	}

	expected := zero.Score(13)
	actual := sc.CalculateScore(hand)

	if actual != expected {
		t.Error("Error")
	}
}
