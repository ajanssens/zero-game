package websocket

import (
	"log"

	"gitlab.com/ajanssens/zero-game/zero"
)

type ScoreService struct{}

func (s *ScoreService) CalculateScore(hand []zero.Card) zero.Score {
	score := 0

	cardsByValue := make(map[int][]string)
	cardsByColor := make(map[string]int)
	var valueComputed []int

	for _, card := range hand {
		cardsByValue[card.Value] = append(cardsByValue[card.Value], card.Color)
		cardsByColor[card.Color] = cardsByColor[card.Color] + 1
	}

	for value, colors := range cardsByValue {
		if len(colors) <= 4 {
			for _, color := range colors {
				if cardsByColor[color] <= 4 && !contains(valueComputed, value) {
					score = score + value
					valueComputed = append(valueComputed, value)
				}
			}
		}
	}

	log.Println("Score calculated")

	return zero.Score(score)
}

func contains(intSlice []int, searchInt int) bool {
	for _, value := range intSlice {
		if value == searchInt {
			return true
		}
	}
	return false
}
