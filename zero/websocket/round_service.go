package websocket

import "gitlab.com/ajanssens/zero-game/zero"

type RoundService struct{}

func (s *RoundService) Create(cards []zero.Card) *zero.Round {
	return &zero.Round{
		PlayerTurn: 0,
		Turn:       0,
		IsFinished: false,
		Cards:      cards,
	}
}

func (s *RoundService) StartRound(round *zero.Round) *zero.Round {
	round.Turn = 1

	return round
}

func (s *RoundService) AddPlayer(round *zero.Round, player zero.Player) *zero.Round {
	round.Players = append(round.Players, player)
	return round
}

func (s *RoundService) SwitchCard(player zero.Player, round zero.Round, cardFromHand int, cardFromTable int) {
	player.Hand[cardFromHand], round.Table[cardFromTable] = round.Table[cardFromTable], player.Hand[cardFromHand]
}

func (s *RoundService) NextTurn(round *zero.Round) *zero.Round {
	if round.LastTurn == round.Turn {
		round.IsFinished = true
		//?
	}

	if round.PlayerTurn == len(round.Players)-1 {
		round.PlayerTurn = 0
	} else {
		round.PlayerTurn = round.PlayerTurn + 1
	}

	round.Turn = round.Turn + 1

	return round
}
