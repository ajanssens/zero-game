package websocket

import (
	"log"
	"math/rand"
	"time"

	"gitlab.com/ajanssens/zero-game/zero"
)

type CardService struct{}

func (s *CardService) Create(value int, color string) *zero.Card {
	return &zero.Card{Value: value, Color: color}
}

func (s *CardService) GenerateAllCards() []zero.Card {
	colors := [7]string{"brown", "red", "yellow", "grey", "pink", "blue", "green"}
	var cards []zero.Card
	for _, color := range colors {
		for i := 1; i <= 8; i++ {
			card := zero.Card{Value: i, Color: color}
			cards = append(cards, card)
		}
	}

	log.Println("Cards generated")
	return shuffle(cards)
}

func shuffle(cards []zero.Card) []zero.Card {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	ret := make([]zero.Card, len(cards))
	perm := r.Perm(len(cards))
	for i, randIndex := range perm {
		ret[i] = cards[randIndex]
	}
	return ret
}
