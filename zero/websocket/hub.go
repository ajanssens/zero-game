package websocket

import (
	"log"

	"gitlab.com/ajanssens/zero-game/zero"
)

type wsMessage struct {
	Client      *WSClient
	MessageType string
	Content     string
}

type Hub struct {
	clients    map[*WSClient]bool
	broadcast  chan string
	receive    chan wsMessage
	register   chan *WSClient
	unregister chan *WSClient
	games      map[string]*zero.Game
	content    string
}

var H = Hub{
	clients:    make(map[*WSClient]bool),
	broadcast:  make(chan string),
	receive:    make(chan wsMessage),
	register:   make(chan *WSClient),
	unregister: make(chan *WSClient),
	games:      make(map[string]*zero.Game),
}

func (h *Hub) Run() {
	for {
		select {
		case c := <-h.register:
			h.clients[c] = true
			log.Println("Client registered")
			break

		case c := <-h.unregister:
			_, ok := h.clients[c]
			if ok {
				delete(h.clients, c)
				close(c.send)
			}
			log.Print("client left")
			break

		case m := <-h.receive:
			handleReceivedMessage(m)
			break

		case m := <-h.broadcast:
			h.content = m
			h.BroadcastMessage()
			break
		}
	}
}

func (h *Hub) BroadcastMessage() {
	for c := range h.clients {
		select {
		case c.send <- []byte(h.content):
			break

		// We can't reach the client
		default:
			close(c.send)
			delete(h.clients, c)
		}
	}
}
