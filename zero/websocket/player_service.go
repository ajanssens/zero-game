package websocket

import "gitlab.com/ajanssens/zero-game/zero"

type PlayerService struct{}

func (s *PlayerService) Create(name string, hand []zero.Card, score zero.Score) *zero.Player {
	return &zero.Player{Name: name, Hand: hand, Score: score}
}
