package websocket

import (
	"log"

	"github.com/segmentio/ksuid"
	"gitlab.com/ajanssens/zero-game/zero"
)

type GameService struct {
	game *zero.Game
}

func (s *GameService) Create(maxPlayers int) *zero.Game {
	id := ksuid.New()
	gameID := zero.GameID(id.String())
	game := zero.Game{ID: gameID, MaxPlayers: maxPlayers}

	gs := GameService{&game}
	rs := RoundService{}
	cs := CardService{}

	round := rs.Create(cs.GenerateAllCards())

	gs.AddRound(round)

	H.games[id.String()] = &game
	log.Println("Game Created with id " + id.String())

	return &game
}

func (s *GameService) AddRound(round *zero.Round) {
	s.game.Rounds = append(s.game.Rounds, round)
}

func (s *GameService) UpdateCurrentRound(game *zero.Game, currentRound int) *zero.Game {
	game.CurrentRound = currentRound
	return game
}

func (s *GameService) StartGame(game *zero.Game, gameStatus bool) *zero.Game {
	game.GameStarted = gameStatus
	return game
}
