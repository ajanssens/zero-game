package main

import (
	"log"
	"net/http"

	"gitlab.com/ajanssens/zero-game/zero/websocket"
)

func main() {
	go websocket.H.Run()
	http.HandleFunc("/ws", websocket.ServeWs)
	log.Fatal(http.ListenAndServe(":3001", nil))
}
